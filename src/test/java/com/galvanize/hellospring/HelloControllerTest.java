package com.galvanize.hellospring;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest({HelloController.class, CalculateController.class, VowelCounter.class, PostSearchReplaceController.class})
public class HelloControllerTest {

  @Autowired
  MockMvc mockMvc;

  @Test
  void sayHello_noArgs_returnsHelloWorld() throws Exception {

    mockMvc.perform(MockMvcRequestBuilders.get("/hello"))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content().string("Hello World"));
  }

  @Test
  void sayHello_stringName_returnsHelloName() throws Exception {

    mockMvc.perform(MockMvcRequestBuilders.get("/hello?name=Paul"))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content().string("Hello Paul"));

  }

  @Test
  void addNumbers_returnsSumOfNumbers() throws Exception {

    mockMvc.perform(MockMvcRequestBuilders.get("/calculate?numbers=3,4&operator=+"))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content().string(String.valueOf(7)));

  }

  @Test
  void subtractNumbers_returnFirstNumberMinusAllOthers() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get("/calculate?numbers=3,4&operator=-"))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content().string(String.valueOf(-1)));
  }

  @Test
  void multiplyNumbers_returnNumbersMultiplied() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get("/calculate?numbers=3,4&operator=*"))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content().string(String.valueOf(12)));
  }

  @Test
  void divideNumbers_returnFirstNumberDividedbyAllOthers() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get("/calculate?numbers=10,2&operator=/"))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content().string(String.valueOf(5)));
  }


  @Test
  void countVowels_returnTotalVowelCountinString() throws Exception {

    mockMvc.perform(MockMvcRequestBuilders.get("/vowels?word=hullabaloo"))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content().string(String.valueOf(5)));

  }


  @Test
  void postStringTest() throws Exception {

    mockMvc.perform(MockMvcRequestBuilders.post("/postString?string=Give it a go"))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content().string("Give it a go"));
  }

  @Test
  void findAndReplace_ifSearchStringFoundReplaceWithReplaceString() throws Exception{

    mockMvc.perform(MockMvcRequestBuilders.post("/postString?string=Give it a go"));

    mockMvc.perform(MockMvcRequestBuilders.patch("/findReplace?find=go&replaceWith=whirl"))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content().string("Give it a whirl"));

  }


}
