package com.galvanize.hellospring;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VowelCounter {

  @GetMapping("/vowels")
  public int countVowels(@RequestParam(defaultValue = "Hey", required = false) String word){

    int count = 0;
    for (char letter: word.toLowerCase().toCharArray()){
      if (letter == 'a' || letter == 'e' || letter == 'i' || letter == 'o' || letter == 'u' || letter == 'y') {
        count++;
      }
    }
    return count;
  }


}
