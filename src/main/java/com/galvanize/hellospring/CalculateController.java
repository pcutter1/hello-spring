package com.galvanize.hellospring;

import java.util.Arrays;
import java.util.stream.Collectors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculateController {

  @GetMapping("/calculate")
  public String calculateNumbers(@RequestParam() int[] numbers, char operator) {
    int value = numbers[0];

    switch (operator) {
      case '+': value = Arrays.stream(numbers).sum();
        break;
      case '-':
        for (int i = 1; i < numbers.length; i++) {
          value -= numbers[i];
        }
        break;
      case '*':
        for (int i = 1; i < numbers.length; i++) {
          value *= numbers[i];
        }
        break;
      case '/':
        for (int i = 1; i < numbers.length; i++) {
          value /= numbers[i];
        }
        break;
      default: value = 0;
    }

      return String.valueOf(value);
  }
}
