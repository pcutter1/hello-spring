package com.galvanize.hellospring;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@ResponseBody
@RestController
public class PostSearchReplaceController {

  @RequestMapping(value = "/postString", method = {RequestMethod.POST})
  public String postString (@RequestParam(defaultValue = "Paul") String string) {
    return string;
  }

  @RequestMapping(value = "/findReplace", method = {RequestMethod.PATCH})
  public String searchAndPatchString (@RequestParam() String find, String replaceWith) {
   return null;
  }

}
